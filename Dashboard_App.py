
import streamlit as st

st.set_page_config(page_title='Demo Dashboard', 
                   layout='wide',
                   page_icon='asset\page_icon.svg')

st.markdown("<h1 style='text-align: center;'>Demo Dashboard run in Docker Container</h1>", unsafe_allow_html=True)

